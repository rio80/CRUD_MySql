﻿# Host: localhost  (Version 5.5.5-10.1.25-MariaDB)
# Date: 2018-01-13 13:11:52
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "telepon"
#

DROP TABLE IF EXISTS `telepon`;
CREATE TABLE `telepon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nomor` varchar(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Data for table "telepon"
#

INSERT INTO `telepon` VALUES (2,'Mars','08576666770'),(3,'Gunawan Aji Sitorang','081901234567'),(4,'Hermansyah Budi','08197654321'),(8,'Rio Firmansyah','081807277431');
