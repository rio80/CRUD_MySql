﻿# Host: localhost  (Version 5.5.5-10.1.25-MariaDB)
# Date: 2018-01-13 13:09:33
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "biodata"
#

DROP TABLE IF EXISTS `biodata`;
CREATE TABLE `biodata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL DEFAULT '-',
  `alamat` varchar(100) NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "biodata"
#

INSERT INTO `biodata` VALUES (1,'Rio','Kranji, Bekasi Barat'),(2,'Budi','Gambir, Jakarta pusat'),(3,'Tia','Ciledug, Tangerang'),(5,'Rizki maulana','Kebayoran, Jakarta pusat'),(6,'Rianti','Kedoya, Jakarta Barat'),(7,'Samsul','Cikarang, Bekasi');

#
# Structure for table "mahasiswa"
#

DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa` (
  `npm` int(10) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `kelas` char(1) DEFAULT NULL,
  `sesi` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`npm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "mahasiswa"
#

INSERT INTO `mahasiswa` VALUES (0,'sigit','',''),(2323,'fffff','d','222'),(10101,'Rio Firmansyah','B','Mobile Programming'),(10102,'Budi Hermansyah','B','Mobile Programming'),(10103,'Rina sahriana','C','Pagi (09.00-11.00 WIB)'),(10104,'Endank Soekamti','C','Siang (13.00-15.00 WIB)'),(10105,'Rini Herliani','A','Siang (13.00-15.00 WIB)'),(10106,'Ramdan nur','M','Siang (13.00-15.00 WIB)');
