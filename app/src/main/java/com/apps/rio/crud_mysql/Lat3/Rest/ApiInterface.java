package com.apps.rio.crud_mysql.Lat3.Rest;

import com.apps.rio.crud_mysql.Lat3.Model.GetKontak;
import com.apps.rio.crud_mysql.Lat3.Model.Kontak;
import com.apps.rio.crud_mysql.Lat3.Model.PostPutDelKontak;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by rio senjou on 02/12/2017.
 */

public interface ApiInterface {

    @GET("kontak")
    Call<GetKontak> getKontak();

    @FormUrlEncoded
    @POST("kontak")
    Call<PostPutDelKontak> postKontak(@Field("nama") String nama,
                                      @Field("nomor") String nomor);

    @FormUrlEncoded
    @PUT("kontak")
    Call<PostPutDelKontak> putKontak(@Field("id") String id,
                                     @Field("nama") String nama,
                                     @Field("nomor") String nomor);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "kontak", hasBody = true)
    Call<PostPutDelKontak> deleteKontak(@Field("id") String id);
}
