package com.apps.rio.crud_mysql.Lat2.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.crud_mysql.Lat2.Data.Result;
import com.apps.rio.crud_mysql.Lat2.Data.Value;
import com.apps.rio.crud_mysql.Lat2.Interface.RegisterAPI;
import com.apps.rio.crud_mysql.Lat2.UpdateActivity;
import com.apps.rio.crud_mysql.Lat2.ViewActivity;
import com.apps.rio.crud_mysql.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemLongClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rio senjou on 27/11/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<Result> results;

    private OnItemClicked onClick;

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public RecyclerViewAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Result result = results.get(position);
        holder.textViewNPM.setText(result.getNpm());
        holder.textViewNama.setText(result.getNama());
        holder.textViewKelas.setText(result.getKelas());
        holder.textViewSesi.setText(result.getSesi());

       /* holder.textViewNPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(position);
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        @BindView(R.id.textNPM)
        TextView textViewNPM;
        @BindView(R.id.textNama)
        TextView textViewNama;
        @BindView(R.id.textKelas)
        TextView textViewKelas;
        @BindView(R.id.textSesi)
        TextView textViewSesi;

        String pilih_npm;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {
            final String npm = textViewNPM.getText().toString();
            final String nama = textViewNama.getText().toString();
            final String kelas = textViewKelas.getText().toString();
            final String sesi = textViewSesi.getText().toString();

            final CharSequence[] dialogItem = {
                    "Update Data", "Delete Data"
            };

            AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(context);
            dialogbuilder.setTitle("Pilihan");
            dialogbuilder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            Intent i = new Intent(context, UpdateActivity.class);
                            i.putExtra("npm", npm);
                            i.putExtra("nama", nama);
                            i.putExtra("kelas", kelas);
                            i.putExtra("sesi", sesi);
                            context.startActivity(i);
                            break;
                        case 1:

                            pilih_npm = npm;

                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setTitle("Peringatan");
                            alertDialogBuilder
                                    .setMessage("Apakah Anda yakin ingin mengapus data dengan NPM= "+pilih_npm+ " ?")
                                    .setCancelable(false)
                                    .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            String npm = pilih_npm;
                                            Retrofit retrofit = new Retrofit.Builder()
                                                    .baseUrl(RegisterAPI.URL)
                                                    .addConverterFactory(GsonConverterFactory.create())
                                                    .build();


                                            RegisterAPI api = retrofit.create(RegisterAPI.class);

                                            Call<Value> call = api.hapus(npm);
                                            call.enqueue(new Callback<Value>() {
                                                @Override
                                                public void onResponse(Call<Value> call, Response<Value> response) {
                                                    String value = response.body().getValue();
                                                    String message = response.body().getMessage();
                                                    if (value.equals("1")) {
                                                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                                                    } else {
                                                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Value> call, Throwable t) {

                                                }
                                            });


                                        }
                                    })


                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            alertDialogBuilder.create().show();
                            break;
                    }
                }
            });
            dialogbuilder.create().show();


            return true;
        }
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }
}
