package com.apps.rio.crud_mysql.Lat3.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rio senjou on 02/12/2017.
 */

public class GetKontak {
    @SerializedName("message")
    private String message;

    @SerializedName("result")
    List<Kontak> listDataKontak;

    @SerializedName("status")
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Kontak> getListDataKontak() {
        return listDataKontak;
    }

    public void setListDataKontak(List<Kontak> listDataKontak) {
        this.listDataKontak = listDataKontak;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
