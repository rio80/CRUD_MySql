package com.apps.rio.crud_mysql.Lat3.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rio.crud_mysql.Lat3.EditActivity;
import com.apps.rio.crud_mysql.Lat3.Model.Kontak;
import com.apps.rio.crud_mysql.R;

import java.util.List;

/**
 * Created by rio senjou on 02/12/2017.
 */

public class KontakAdapter extends RecyclerView.Adapter<KontakAdapter.MyViewHolder> {
    List<Kontak> mKontakList;

    public KontakAdapter(List<Kontak> mKontakList) {

        this.mKontakList = mKontakList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lat3_kontak_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Kontak item=mKontakList.get(position);
        holder.mTextViewId.setText("Id = "+item.getId());
        holder.mTextViewNama.setText("Nama = "+item.getNama());
        holder.mTextViewNomor.setText("Nomor = " + item.getNomor());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(v.getContext(), EditActivity.class);
                mIntent.putExtra("Id", mKontakList.get(position).getId());
                mIntent.putExtra("Nama", mKontakList.get(position).getNama());
                mIntent.putExtra("Nomor", mKontakList.get(position).getNomor());
                v.getContext().startActivity(mIntent);

            }
        });
    }


    @Override
    public int getItemCount() {

        return mKontakList.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewId, mTextViewNama, mTextViewNomor;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewId=(TextView)itemView.findViewById(R.id.tvId);
            mTextViewNama=(TextView)itemView.findViewById(R.id.tvNama);
            mTextViewNomor=(TextView)itemView.findViewById(R.id.tvNomor);
        }
    }
}
