package com.apps.rio.crud_mysql.Lat2;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.apps.rio.crud_mysql.Lat2.Data.Value;
import com.apps.rio.crud_mysql.Lat2.Interface.RegisterAPI;
import com.apps.rio.crud_mysql.R;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends AppCompatActivity {
    private String URL = RegisterAPI.URL;
    private RadioButton radioSexButton;
    private ProgressDialog progress;
    private Toolbar toolbar;
    @BindView(R.id.radioSesi)
    RadioGroup radioGroup;
    @BindView(R.id.editTextNPM)
    EditText editTextNPM;
    @BindView(R.id.editTextNama)
    EditText editTextNama;
    @BindView(R.id.editTextKelas)
    EditText editTextKelas;

    @OnClick(R.id.buttonDaftar)
    void daftar() {
        //lakukan pengecekan apabila ada kolom yang kosong
       if(false==cek_form()){
           Toast.makeText(getApplicationContext(),"Isikan data yang kosong",Toast.LENGTH_SHORT).show();
       }else{
           //membuat progress dialog
           progress = new ProgressDialog(this);
           progress.setCancelable(false);
           progress.setMessage("Loading ...");
           progress.show();

           //mengambil data dari edittext
           String npm = editTextNPM.getText().toString();
           String nama = editTextNama.getText().toString();
           String kelas = editTextKelas.getText().toString();

           int selectedId = radioGroup.getCheckedRadioButtonId();
           // mencari id radio button
           radioSexButton = (RadioButton) findViewById(selectedId);
           String sesi = radioSexButton.getText().toString();

           Retrofit retrofit = new Retrofit.Builder()
                   .baseUrl(URL)
                   .addConverterFactory(GsonConverterFactory.create())
                   .build();

           RegisterAPI api = retrofit.create(RegisterAPI.class);
           Call<Value> call = api.daftar(npm, nama, kelas, sesi);

           call.enqueue(new Callback<Value>() {
               @Override
               public void onResponse(Call<Value> call, Response<Value> response) {

                   String value = response.body().getValue();
                   String message = response.body().getMessage();

                   progress.dismiss();

                   if (value.equals("1")) {
                       Toast.makeText(Main2Activity.this, message, Toast.LENGTH_SHORT).show();
                   } else {
                       Toast.makeText(Main2Activity.this, message, Toast.LENGTH_SHORT).show();
                   }

               }

               @Override
               public void onFailure(Call<Value> call, Throwable t) {
                   progress.dismiss();
                   Toast.makeText(Main2Activity.this,
                           "Jaringan Error!\n"+t.getMessage().toString(),
                           Toast.LENGTH_SHORT).show();
               }
           });
       }
    }

    @OnClick(R.id.buttonLihat)
    void lihat() {
        startActivity(new Intent(Main2Activity.this, ViewActivity.class));
    }

    public boolean cek_form() {
        if (TextUtils.isEmpty(editTextNPM.getText()) ||
                TextUtils.isEmpty(editTextNama.getText()) ||
                TextUtils.isEmpty(editTextKelas.getText())) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Form Register Mahasiswa");
    }


}
