package com.apps.rio.crud_mysql.Lat1.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apps.rio.crud_mysql.R;
import com.apps.rio.crud_mysql.Lat1.data.Data;

import java.util.List;

/**
 * Created by rio senjou on 26/11/2017.
 */

public class Adapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Data> items;

    public Adapter(Activity activity, List<Data> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater==null){
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.list_row,null);
        }
        TextView id,nama,alamat;
        id=(TextView)convertView.findViewById(R.id.id);
        nama=(TextView)convertView.findViewById(R.id.nama);
        alamat=(TextView)convertView.findViewById(R.id.alamat);

        Data item=items.get(position);

        id.setText(item.getId());
        nama.setText(item.getNama());
        alamat.setText(item.getAlamat());

        return convertView;
    }
}
