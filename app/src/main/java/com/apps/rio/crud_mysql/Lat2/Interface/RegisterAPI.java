package com.apps.rio.crud_mysql.Lat2.Interface;

import com.apps.rio.crud_mysql.Lat2.Data.Value;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by rio senjou on 27/11/2017.
 */

public interface RegisterAPI {

    //URL dari tethering Smartphone(LGK430) ke PC
//    String URL = "http://192.168.43.123:81/crud_mysql/Lat2/";

    //URL dari WIFI PC(Connectify-me) ke Smartphone
    String URL = "http://192.168.58.1:81/crud_mysql/Lat2/";

    @FormUrlEncoded
    @POST("insert.php")
    Call<Value> daftar(@Field("npm") String npm,
                       @Field("nama") String nama,
                       @Field("kelas") String kelas,
                       @Field("sesi") String sesi);

    @GET("select.php")
    Call<Value> view();

    @FormUrlEncoded
    @POST("update.php")
    Call<Value> ubah(@Field("npm") String npm,
                     @Field("nama") String nama,
                     @Field("kelas") String kelas,
                     @Field("sesi") String sesi);


    @FormUrlEncoded
    @POST("delete.php")
    Call<Value> hapus(@Field("npm") String npm);

    @FormUrlEncoded
    @POST("search.php")
    Call<Value> cari(@Field("search") String search);
}
