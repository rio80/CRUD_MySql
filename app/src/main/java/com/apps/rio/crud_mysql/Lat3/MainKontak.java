package com.apps.rio.crud_mysql.Lat3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.apps.rio.crud_mysql.Lat3.Adapter.KontakAdapter;
import com.apps.rio.crud_mysql.Lat3.Model.GetKontak;
import com.apps.rio.crud_mysql.Lat3.Model.Kontak;
import com.apps.rio.crud_mysql.Lat3.Rest.ApiClient;
import com.apps.rio.crud_mysql.Lat3.Rest.ApiInterface;
import com.apps.rio.crud_mysql.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainKontak extends AppCompatActivity {

    Button btIns;
    ApiInterface apiInterface;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    public static MainKontak ma;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_kontak);

        btIns=(Button)findViewById(R.id.btIns);
        btIns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainKontak.this, InsertActivity.class));
            }
        });

        recyclerView=(RecyclerView)findViewById(R.id.recyclerViewKontak);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        ma=this;
        refresh();


    }

    public void refresh(){
        Call<GetKontak> kontakCall=apiInterface.getKontak();
        kontakCall.enqueue(new Callback<GetKontak>() {
            @Override
            public void onResponse(Call<GetKontak> call, Response<GetKontak> response) {
                List<Kontak> kontakList=response.body().getListDataKontak();
                Log.d("Retrofit Get", "Jumlah data Kontak: " +
                        String.valueOf(kontakList.size()));
                adapter=new KontakAdapter(kontakList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<GetKontak> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}
