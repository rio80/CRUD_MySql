package com.apps.rio.crud_mysql.Lat2.Data;

/**
 * Created by rio senjou on 27/11/2017.
 */

public class Result {

    String npm;
    String nama;
    String kelas;
    String sesi;

    public String getNpm() {
        return npm;
    }

    public String getNama() {
        return nama;
    }

    public String getKelas() {
        return kelas;
    }

    public String getSesi() {
        return sesi;
    }

}
