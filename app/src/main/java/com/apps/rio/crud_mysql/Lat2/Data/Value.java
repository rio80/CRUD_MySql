package com.apps.rio.crud_mysql.Lat2.Data;

import java.util.List;

/**
 * Created by rio senjou on 27/11/2017.
 */

public class Value {
    String value;
    String message;
    List<Result> result;

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public List<Result> getResult() {
        return result;
    }
}
